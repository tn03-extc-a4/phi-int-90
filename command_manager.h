#ifndef COMMAND_MANAGER_H // TO INLCUDE THE HEADER ONLY ONCE 

#define COMMAND_MANAGER_H   //header guard

// demo checking if the env. is 32 bits or 64bits 
#ifdef __x86_64__  // checking if env 64 bits
#include "config_64.h"
#elif __i386__ // checking if env 32 bits 
#include "config_32.h"
#else 
#define NO_PROC // if not both defining no_processor
#endif 


#ifndef NO_PROC //  checking if no_processor defined , if not defined everything under this will be declared


#include<stdbool.h>
#include<stdlib.h>

#define OK 1           // all ok
#define NOT_OK -1      // something went wrong 
#define NOT_INIT -2    // if manager not initalized
#define NOT_EMPTY -3   // if register full
#define NOT_FOUND -4   // if command not found in the register 

/* for testing out command manager dummy commands */
// typedef int16_t proj_status ; // project status for command execution functions 
typedef int (*execution_fp) (void*, int) ; // command specific execution function 

typedef struct _command_t{
    command_id c_id ;
    char* name ;
    execution_fp exec ;
} _command_t ;


/* 
    the command type object/ command object will be created by the "command object" API the api will also bind the function required to execute the command object in
    the command object structure itself , this command type object _command_t will we registered in the command regsiter which will be  initialized when we initalize the 
    command manager using the command_manager_init function 
*/



/* Command Manager header file will contain the following functions  */
/*---------------<START>-----------------*/

// typedef int16_t manager_status ; 
/* 
    "manager_status" is the status that will be returned by the command manager functions after execution of a certain function
*/ 


typedef unsigned int no_of_commands ;
/* 
    "no_of_commands" will be passed to command manager intialize function to get initial address register of size as same as number of commands 
*/


typedef _command_t** command_reg ;
/* 
    "command_reg" is a regsiter to store/regsiter the commands by any api implementing the command manager header file 
    "command_reg" will be a commnd type double pointer to hold the command object being regsitered 
*/ 

command_reg manager_init(command_reg, no_of_commands) ; 
/*
    "manager_init" function will take no_of_commands as an argument and based on the number of commands will initalize a register of size same as the numer of commands
    it will return a command type double pointer i.e. the command register 
*/

int manager_destroy_register(command_reg*) ; 
/* 
    "manager_destroy_register" will take the address of the command register to be destroyed 
    after deleting/destroying the command register it will set it to NULL
*/

int manager_register_command(command_reg, _command_t*) ; 
/*
    "manager_register_command" will take the command_reg and the command type pointer so that it can register the command 
    passed inside the register passed to the function the function itself consists of an incrementer such that the very first command 
    that is registered will be at the top of the register and so on
*/

bool command_in_register(command_reg, no_of_commands, command_id) ;
/*
    following function will return a boolean value depending if the command is present in the register or not 
*/

_command_t* manager_get_command(command_reg, no_of_commands, command_id) ; 
/*
    "manager_get_command" will take the command id(command_id) , number of commands(no_of_commands) and command register(command_reg) as argument 
    so that it can iterate over number of commands inside the command register and return the command as per the command id sent to it 
    else if the command not present it will return command "NOT_FOUND" status 
*/

int manager_remove_command(command_reg, no_of_commands, _command_t*) ; 
/*
    "manager_remove_command" will be called after the user has executed the command which was registered in the command register 
    this function will remove that specific command from the register itself this will empty space for another command to be registered or 
    to register a modified command 
*/
#endif

#endif
/*---------------<END>-----------------*/